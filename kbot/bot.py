#!/usr/bin/env python3
import asyncio
import datetime
import inspect
import json
import logging
import os
import re
import sys
from concurrent.futures import CancelledError
from copy import deepcopy
from csv import DictReader, DictWriter
from os import getpid
from pathlib import Path
from shutil import rmtree
from time import sleep
from typing import List, Generator

import psutil
import pykeybasebot
from git import Actor, GitCommandError, Repo
from pykeybasebot.types import chat1
from setproctitle import setproctitle

from decorators import IsChatMsg as is_chat_msg
from decorators import IsSystemMsg as is_system_msg
from excepts import ActionError
from utils import force_async

# TODO: Split this on little regexes
FIX_SENTENCE_POSIBILITIES = [
    r'^(?P<fix_command>brb|hi|back|hello|bye)\s(?P<fix_hour>[0-1]?[0-9]|2[0-3]):(?P<fix_minute>[0-5][0-9])(?:(?:\:)?(?P<fix_second>[0-5][0-9])?)(?P<fix_sentence>.*)', # noqa
    r'^(?P<fix_command>brb|hi|back|hello|bye)\s(?P<fix_year>\d{4})\-(?P<fix_month>0?[1-9]|1[012])\-(?P<fix_day>0?[1-9]|[12][0-9]|3[01])\s(?P<fix_hour>[0-1]?[0-9]|2[0-3]):(?P<fix_minute>[0-5][0-9])(?:(?:\:)?(?P<fix_second>[0-5][0-9])?)(?P<fix_sentence>.*)' # noqa
    r'^(?P<fix_command>brb|hi|back|hello|bye)\s(?P<fix_hour>[0-1]?[0-9]|2[0-3]):(?P<fix_minute>[0-5][0-9])(?:(?:\:)?(?P<fix_second>[0-5][0-9])?)\s(?P<fix_day>0?[1-9]|[12][0-9]|3[01])\-(?P<fix_month>0?[1-9]|1[012])\-(?P<fix_year>\d{4})(?P<fix_sentence>.*)' # noqa
]

TEAM_NAME_REGEXP = re.compile(r'^[a-z]+(_[a-z0-9]+)*[a-z0-0]$')

if "win32" in sys.platform:
    # Windows specific event-loop policy
    asyncio.set_event_loop_policy(
        asyncio.WindowsProactorEventLoopPolicy()  # type: ignore
    )

logfile = Path(__file__).parent.joinpath('app.log')

logging.basicConfig(level=logging.INFO,
                    filename=str(logfile),
                    filemode='w',
                    format='%(name)s - %(levelname)s - %(message)s')


async def async_iter(iterable) -> Generator:
    """
    Converts any iterable into an async iterator.
    """
    for iter_item in iterable:
        yield iter_item


class KBot:

    log = logging.getLogger(__name__)
    messages = {}

    def __init__(self, botname, paperkey):
        self.botname = botname
        self.paperkey = paperkey
        self._commands = []

    def run(self):
        setproctitle('kbot')
        self._bot = pykeybasebot.Bot(
            username=self.botname, paperkey=self.paperkey, handler=self.command_handler
        )
        self.loop = asyncio.new_event_loop()
        try:
            self.loop.run_until_complete(self._bot.start({}))
        except CancelledError:
            self.log.info('Cancelled')
        finally:
            self.loop.close()

    def add_command(self, trigger_func, command_func):
        async_command = command_func
        if not inspect.iscoroutinefunction(command_func):
            async_command = force_async(command_func)
        self._commands.append(
            {"trigger_func": trigger_func, "command_func": async_command}
        )

    def store_event(self, event: pykeybasebot.kbevent.KbEvent):
        if not hasattr(event, 'msg'):
            return None
        team_name = event.msg.channel.name
        topic_name = event.msg.channel.topic_name
        self.messages.setdefault(f'{team_name}#{topic_name}', [])
        self.messages[f'{team_name}#{topic_name}'].append(event)

    async def say(self, team_name: str, message: str, topic_name: str = None):
        # make a new loop to run the async bot.chat.send method in what  is currently
        # a synchronous context
        async def func():
            result = await self._bot.chat.send(team_name, message)
            self.store_event(result)
        self.loop.create_task(func())

    async def reply(self, event, message: str):
        # make a new loop to run the async bot.chat.send method in what is currently
        # a synchronous context
        async def func():
            result = await self._bot.chat.send(event.msg.channel, message)
            self.store_event(result)
        self.loop.create_task(func())

    async def delete(self, channel: chat1.ChatChannel, message_id: str):
        # make a new loop to run the async bot.chat.excecute method in what is currently
        # a synchronous context
        delete_order = {
            "method": "delete",
            "params": {
                "options": {"channel": channel.to_dict(), "message_id": message_id}
            },
        }
        # loop = asyncio.new_event_loop()
        # asyncio.set_event_loop(loop)
        # result = loop.run_until_complete(self._bot.chat.execute(delete_order))
        result = await self._bot.chat.execute(delete_order)
        return result

    async def leave(self, channel: chat1.ChatChannel, permanent: bool = False):
        leave_order = {
            "method": "leave-team",
            "params": {
                "options": {
                    "team": channel.name,
                    "permanent": permanent
                }
            }
        }

        self.loop.create_task(self._bot.submit('team api', input_data=json.dumps(leave_order).encode()))

    async def command_handler(self, bot, event: pykeybasebot.kbevent.KbEvent):
        for possibility in self._commands:
            if possibility["trigger_func"](event):
                return self.loop.create_task(possibility["command_func"](event))

    async def read_groups(self, group_name: str, revision: int = None) -> List[str]:
        namespace = 'config'
        team_name = f'{self.botname},{self.botname}'
        res = await self._bot.kvstore.get(namespace, group_name, team=team_name)
        if not res.entry_value:
            return []

        try:
            return json.loads(res.entry_value)
        except BaseException as excp:
            self.log.exception(excp, extra={
                'entry_value': res.entry_value,
                'group_name': group_name,
                'tean_name': team_name,
                'namespace': namespace
            })
            return []

    async def groupadd(self, group_name: str, force: bool = False) -> bool:
        if not isinstance(group_name, str):
            excp = TypeError(f'{group_name} is not a valid string')
            self.log.exception(excp, extra={
                'group_name': group_name,
                'force': force,
                'variable_type': type(group_name)
            })
            return False

        namespace = 'config'
        team_name = f'{self.botname},{self.botname}'
        groups = await self.read_groups(group_name)
        groups.append(group_name)
        await self._bot.kvstore.put(namespace, team=team_name, entry_key=group_name, entry_value=json.dumps(groups))
        return True

    async def groupdel(self, group_name: str) -> bool:
        if not isinstance(group_name, str):
            excp = TypeError(f'{group_name} is not a valid string')
            self.log.exception(excp, extra={
                'group_name': group_name,
                'variable_type': type(group_name)
            })
            return False

        namespace = 'config'
        team_name = f'{self.botname},{self.botname}'
        groups = await self.read_groups(group_name)
        if group_name not in groups:
            return True
        groups.remove(groups.index(group_name))
        self.loop.create_task(self._bot.kvstore.put(namespace, team=team_name, entry_key=group_name, entry_value=json.dumps(groups)))
        return True


class CustomBot(KBot):

    repos_path = Path.cwd().joinpath('repos').resolve()
    _available_commands = ['!bye', '!hi', '!hello', '!brb', '!back']
    _extra_commands = ['!help', '!oops']

    def get_used_path(self, team_name: str):
        first_letter, second_letter, third_letter = team_name[0], team_name[1], team_name[2]
        return self.repos_path.joinpath(first_letter).joinpath(second_letter).joinpath(third_letter) \
                              .joinpath(team_name[3:])

    async def find_fix_sentences(self, sentence: str):
        async for item in async_iter(FIX_SENTENCE_POSIBILITIES):
            match = re.compile(item).match(sentence)
            if match:
                yield match

    async def setup_notifications(self, repo_name: str, team_name: str, topic_name: str = None):
        command = ['git', 'settings']

        enabled = False

        if topic_name:
            enabled = True
            command.extend(['--channel', topic_name])
        else:
            command.append('--disable-chat')

        command.extend([repo_name, team_name])

        result = await self._bot.submit(' '.join(command))
        if not bool(result):
            return False, (f'Cannot change settings {team_name}/{repo_name} #{topic_name} due to missing permissions.')

        disabled_msg = f'Notifications for {team_name}/{repo_name} disabled on #{topic_name}'
        enabled_msg = f'Notifications for {team_name}/{repo_name} enabled on #{topic_name}'
        msg = enabled_msg if enabled else disabled_msg

        return True, msg

    def get_existing_repo_url(self, repo_name, username=None, team_name=None):
        if not username and not team_name:
            raise ValueError('Either username or team_name must be provided.')

        if not team_name:
            return f'keybase://private/{username}/{repo_name}'
        return f'keybase://team/{team_name}/{repo_name}'

    async def create_repo(self, repo_name, team_name=None, force=False):

        command = ['git', 'create']
        repo_url = None
        already_exists = False
        force_str = ''
        msg_str = ''
        repo_url = self.get_existing_repo_url(repo_name, team_name=team_name)

        if team_name:
            command.extend(['--team', team_name])
        command.append(repo_name)

        command_str = ' '.join(command)
        result = await self._bot.submit(command_str)
        if not result and not force:
            already_exits = True
            repo_url = None

        if force and already_exists:
            force_str = 'But we will use it anyway.' if force else ''
            return repo_url, force_str

        msg_str = f' The repo {repo_url} already exists. {force_str}'.strip()
        msg_str = msg_str if already_exists else None

        return repo_url, msg_str

    def check_valid_team_name(self, team_name):
        subteams = team_name.split('.')
        word = 'Team' if len(subteams) == 1 else 'Subteams'
        for subteam in team_name.split('.'):
            if len(subteam) < 3 or len(subteam) > 16:
                return False, 'Team names must be between, 3 and 16 characters long.'

            matches = TEAM_NAME_REGEXP.match(subteam)
            if not matches:
                reason = f"{word} names must be letters (a-z), numbers, and underscores." + \
                    "Also, they can't start with underscores or use double underscores, to avoid confusion."
                return False, reason
        return True, ''

    async def create_channel(self, team_name, channel_name, topic_type=None):

        command = ['chat', 'create-channel']
        topic_type = topic_type or 'chat'

        if topic_type:
            command.extend(['--topic-type', topic_type])

        command.extend([team_name, channel_name])

        is_valid, reason = self.check_valid_team_name(team_name)
        if not is_valid:
            return False, reason

        output = await self._bot.submit(' '.join(command))
        if 'success' not in output.lower():
            self.log.error(output)
            return False, 'There was an error creating the channel.'

        return True, f'We have created a channel called {channel_name} in {team_name} group.'

    def clone_repo(self, repo_url, folder_path, force=False):

        # TODO CHECK GET USED PATH
        if force and folder_path.exists():
            rmtree(folder_path)
        folder_path.mkdir(exist_ok=True, parents=True)
        Repo.clone_from(repo_url, folder_path)

    async def register_company(self, team_name, force=False):
        repo_name = 'worklog_hours'
        repo_url, reason = await self.create_repo(repo_name, team_name=team_name, force=force)

        if not repo_url:
            return {'status': 'error', 'messages': [reason]}

        repo_path = self.get_used_path(team_name)
        self.clone_repo(repo_url, repo_path, force=force)

        messages = []
        mandatory_channels = ['WorkHoursPushLog', 'general']
        for item in mandatory_channels:
            is_created, message = await self.create_channel(team_name, item)
            messages.append(message)

        _can_create, message = await self.setup_notifications(repo_name, team_name, topic_name='WorkHoursPushLog')
        messages.append(message)

        messages = []

        status = 'warning' if len(messages) != len(mandatory_channels) else 'ok'
        return {'status': status, 'messages': messages}

    def is_registered_company(self, team_name):
        return Path(self.get_used_path(team_name)).exists()

    async def do_commit(self, event, repo_file):
        team_name = event.msg.channel.name
        username = event.msg.sender.username
        device_id = event.msg.sender.device_id
        device_name = event.msg.sender.device_name
        commit = f'Signed by {username} on {device_name} ({device_id})'
        repo_path = self.get_used_path(team_name)
        repo = Repo(repo_path)

        try:
            repo.remote('origin').pull('master')
        except GitCommandError as err:
            self.log.warning(str(err.stderr))

        if not repo.untracked_files and not repo.is_dirty():
            return {'status': 'ok', 'event': event}

        repo.index.add([str(repo_file)])
        repo.index.commit(commit, author=Actor(username, f'{username}@{device_name}'))
        self.log.info(commit)
        try:
            repo.remote('origin').push('master')
        except GitCommandError as excp:
            self.log.exception(excp)
            return {'status': 'error', 'event': event}
        return {'status': 'ok', 'event': event}

    def read_timestamp_file(self, repo_file):
        if not repo_file.exists():
            return []
        rows = []

        with open(repo_file, 'r') as csv_file:
            csv_file.seek(0)
            lines = csv_file.readlines()
            return [dict(zip(lines[0].split(','), lines[-1].split(',')))]

        # DictReader()
        # with open(repo_file, 'r') as csv_file:
        #     rows = [
        #         row for row in DictReader(csv_file)
        #     ]
        #     csv_file.close()
        #     return rows

    def get_repo_file(self, event, timestamp):
        used_date = datetime.datetime.fromtimestamp(int(timestamp))
        username = event.msg.sender.username
        month = str(used_date.month).zfill(2)
        return self.get_used_path(event.msg.channel.name).joinpath(username).joinpath(f'{used_date.year}{month}.csv')

    def get_stored_timestamps(self, event, used_date):
        repo_file = self.get_repo_file(event, used_date.timestamp())
        return self.read_timestamp_file(repo_file)

    def append_action(self, event, repo_file, row_dict):
        create_header = not repo_file.exists()
        repo_file.parent.mkdir(exist_ok=True)

        with open(repo_file, 'a') as csv_file:
            field_names = ['timestamp', 'action', 'datetime', 'message', 'device_id', 'device_name']
            writer = DictWriter(csv_file, field_names, delimiter=',')
            if create_header:
                writer.writeheader()
            writer.writerow(row_dict)
            csv_file.close()

    async def create_action(self, event, action, message=None, timestamp=None):

        if not action:
            raise ActionError('No action provided')

        timestamp = int(timestamp or event.msg.sent_at)
        repo_file = self.get_repo_file(event, timestamp)

        message = message or ''
        self.append_action(event, repo_file, {
            'timestamp': timestamp,
            'action': action,
            'message': message,
            'datetime': datetime.datetime.fromtimestamp(timestamp).isoformat(),
            'device_id': event.msg.sender.device_id,
            'device_name': event.msg.sender.device_name
        })
        return await self.do_commit(event, repo_file)

    async def get_datetime_and_action(self, sentence, selected_datetime):

        command_actions = {
            'brb': 'BREAK',
            'bye': 'LOGOUT',
            'back': 'BACK',
            'hi': 'LOGIN',
            'hello': 'LOGIN'
        }

        def try_group(matches, group):
            try:
                return int(matches.group(group))
            except BaseException:
                return None

        def build_datetime(matches):
            fix_command = matches.group('fix_command')
            fix_year = try_group(matches, 'fix_year')
            fix_month = try_group(matches, 'fix_month')
            fix_day = try_group(matches, 'fix_day')
            fix_hour = try_group(matches, 'fix_hour')
            fix_minutes = try_group(matches, 'fix_minute')
            fix_seconds = try_group(matches, 'fix_second') or 0
            fix_sentence = matches.group('fix_sentence') or ''

            fixed_datetime = datetime.datetime(selected_datetime.year, selected_datetime.month,
                                               selected_datetime.day, fix_hour, fix_minutes,
                                               fix_seconds, 0)
            if fix_year:
                fixed_datetime = datetime.datetime(fix_year, fix_month, fix_day, fix_hour, fix_minutes, fix_seconds, 0)
            return fixed_datetime, command_actions[fix_command], fix_sentence

        matches = self.find_fix_sentences(sentence)
        async for item in matches:
            return build_datetime(item)


    def can_do_action(self, event: pykeybasebot.kbevent.KbEvent, action: str, timestamp: int = None):

        timestamp = timestamp or event.msg.sent_at
        selected_datetime = datetime.datetime.fromtimestamp(timestamp)
        previous_month_date = deepcopy(selected_datetime).replace(day=1) - datetime.timedelta(days=1)

        last_month_stamps = self.get_stored_timestamps(event, previous_month_date)
        current_month_stamps = self.get_stored_timestamps(event, selected_datetime)
        stored_timestamps = deepcopy(last_month_stamps)
        stored_timestamps.extend(current_month_stamps)

        username = event.msg.sender.username

        actions = {
            'LOGIN': 'LOGOUT',
            'BREAK': 'BACK',
            'LOGOUT': 'LOGIN',
            'BACK': 'BREAK'
        }

        last_action = None
        last_datetime = None
        last_timestamp = None
        if stored_timestamps:
            last_timestamp = stored_timestamps[-1]
            last_datetime = datetime.datetime.fromtimestamp(int(last_timestamp['timestamp']))
            last_action = last_timestamp['action']

        if stored_timestamps and last_datetime and last_datetime > selected_datetime:
            return False, f"@{username} Spacetime manipulation has been detected, incident will be reported."

        elif stored_timestamps and selected_datetime > datetime.datetime.utcnow():
            return False, f"@{username} Spacetime manipulation has been detected, incident will be reported."

        elif stored_timestamps and last_action == action:
            last_time = last_datetime if last_datetime.date() == selected_datetime.date() else timestamp.strftime('%Y-%m-%d %H:%M')
            return False, (f"Ey, @{username} you can\'t do '{action}', two times in a row, you already did that on "
                           f"{last_time}, please do {actions[action]} first or fixit by !oops.")

        elif action == 'LOGIN':
            if not stored_timestamps or last_action == 'LOGOUT':
                return True, f"Welcome @{username}!"
            return False, f"@{username} you need to logout first."

        elif action == 'LOGOUT':
            if not stored_timestamps:
                return False, f"@{username} you need to login first."
            elif last_action in ['BACK', 'LOGIN']:
                return True, f"Good bye @{username}."
            return False, f"You should go !back first @{username}."

        elif action == 'BACK':
            if stored_timestamps and last_action == 'BREAK':
                return True, f"Welcome back @{username}"
            return False, f"So... @{username} back from where exactly?"

        elif action == 'BREAK':
            if last_action in ['LOGIN', 'BACK']:
                return True, f"NP @{username}."

            elif last_action == 'BREAK':
                return True, f"You are already in a break @{username}."

            return False, f"@{username} you need to login first."

        elif action == 'OOPS':
            return True, f'@{username} Time travel succesfully'

        self.log.exception(ActionError('Unexpected combination of actions'), extra={
            'last_action': last_action,
            'action': action,
            'username': username,
            'company': event.msg.channel.name,
            'selected_datetime': selected_datetime
        })

        return False, 'Not implemented, please contact developers.'

    @is_system_msg
    def is_subteam_invite(self, event: pykeybasebot.kbevent.KbEvent):
        return '.' in event.msg.channel.name and event.msg.channel.members_type == 'team'

    @is_system_msg
    def is_team_invite(self, event: pykeybasebot.kbevent.KbEvent):
        return '.' not in event.msg.channel.name and event.msg.channel.members_type == 'team'

    @is_chat_msg
    def is_a_hi(self, event: pykeybasebot.kbevent.KbEvent):
        self.store_event(event)
        return event.msg.content.text.body.startswith(('!hi', '!hello'))

    @is_chat_msg
    def is_a_leave(self, event: pykeybasebot.kbevent.KbEvent):
        self.store_event(event)
        return event.msg.content.text.body.startswith(('!leave'))

    @is_chat_msg
    def is_a_brb(self, event: pykeybasebot.kbevent.KbEvent):
        return event.msg.content.text.body.startswith(('!brb'))

    @is_chat_msg
    def is_a_back(self, event: pykeybasebot.kbevent.KbEvent):
        return event.msg.content.text.body.startswith(('!back'))

    @is_chat_msg
    def is_a_bye(self, event: pykeybasebot.kbevent.KbEvent):
        return event.msg.content.text.body.startswith(('!bye'))

    @is_chat_msg
    def is_a_oops(self, event: pykeybasebot.kbevent.KbEvent):
        return event.msg.content.text.body.startswith(('!oops'))

    @is_chat_msg
    def is_a_register(self, event: pykeybasebot.kbevent.KbEvent):
        return event.msg.content.text.body.startswith(('!register'))

    @is_chat_msg
    def is_a_exit(self, event: pykeybasebot.kbevent.KbEvent):
        return event.msg.content.text.body.startswith(('!exit'))

    async def leave_team(self, event: pykeybasebot.kbevent.KbEvent):
        return await self.leave(event.msg.channel)
        # _teams = await self.read_groups('teams')
        # if event.msg.channel not in _teams:
            # self.say(f'{self.botname},{event.msg.sender.username}', 'Due to data protection policy enforcement.')
            # self.say(f'{self.botname},{event.msg.sender.username}', 'Please, read more at: XXXXX')

    def answer_welcome(self, task, *args, **kwargs):
        result = task.result()
        event = result['event']
        if result['status'] == 'ok':
            self.loop.create_task(self.reply(event, f'Welcome @{event.msg.sender.username}'))
        else:
            self.loop.create_task(self.reply(event, 'There was an error'))

    async def do_welcome(self, event: pykeybasebot.kbevent.KbEvent):
        can_do, reason = self.can_do_action(event, 'LOGIN')
        # return await self.reply(event, 'lol')
        if not can_do:
            return await self.reply(event, reason)
            # await self.reply(event, reason)
        future = self.loop.create_task(self.create_action(event, 'LOGIN'))
        future.add_done_callback(self.answer_welcome)
        self.log.info('We should now do welcome')

    async def do_break(self, event: pykeybasebot.kbevent.KbEvent):
        can_do, reason = self.can_do_action(event, 'BREAK')
        if not can_do:
            return await self.reply(event, reason)
        self.create_action(event, 'BREAK')
        await self.reply(event, reason)
        self.log.info('we should do now LOGIN')

    async def do_back(self, event: pykeybasebot.kbevent.KbEvent):
        can_do, reason = self.can_do_action(event, 'BACK')
        if not can_do:
            return await self.reply(event, reason)
        self.create_action(event, 'BACK')
        self.loop.create_task(self.reply(event, reason))
        self.log.info('we should do now BACK')

    def time_travel(self, task):
        result = task.result()
        event = result['event']
        if result['status'] == 'ok':
            self.loop.create_task(self.reply(event, f'@{event.msg.sender.username} time travel succesfull'))
        else:
            self.loop.create_task(self.reply(event, f'@{event.msg.sender.username} timespace violation'))

    async def do_fix(self, event: pykeybasebot.kbevent.KbEvent):
        can_do, reason = self.can_do_action(event, 'OOPS')
        if not can_do:
            return await self.reply(event, reason)
        sentence = event.msg.content.text.body.split('!oops ')[1]
        # matches = await self.find_fix_sentences(sentence)
        # message = matches.group('fix_sentence')
        selected_datetime = datetime.datetime.fromtimestamp(int(event.msg.sent_at))
        # fixed_datetime, fixed_action = self.get_datetime_and_action(sentence, selected_datetime)

        try:
            fixed_datetime, fixed_action, fix_sentence = await self.get_datetime_and_action(sentence, selected_datetime)
        except ValueError:
            return await self.reply(event, f'Please, @{event.msg.sender.username}, your time string is not valid')

        if fixed_datetime < selected_datetime:
            task = self.loop.create_task(self.create_action(event, fixed_action, message=fix_sentence,
                                         timestamp=fixed_datetime.timestamp()))
            task.add_done_callback(self.time_travel)
            return
        await self.reply(event, reason)

    def answer_bye(self, task, *args, **kwargs):
        result = task.result()
        event = result['event']
        if result['status'] == 'ok':
            self.loop.create_task(self.reply(event, f'Bye @{event.msg.sender.username}'))
        else:
            self.loop.create_task(self.reply(event, 'There was an error'))

    async def do_bye(self, event: pykeybasebot.kbevent.KbEvent):
        self.log.info('step 1')
        can_do, reason = self.can_do_action(event, 'LOGOUT')
        self.log.info('step 2')
        if not can_do:
            return await self.reply(event, reason)
        self.log.info('step 3')
        future = self.loop.create_task(self.create_action(event, 'LOGOUT'))
        future.add_done_callback(self.answer_bye)
        self.log.info('step 4')
        self.log.info('step 5')

    async def do_register(self, event: pykeybasebot.kbevent.KbEvent):
        team_name = event.msg.content.text.body.split('!register')[1].strip()
        company_split = team_name.split(' ')
        team_name = company_split[0]
        force = True if len(company_split) >= 2 and company_split[1] == 'force' else False
        if not team_name:
            await self.reply(event, f'@{event.msg.sender.username} This bot only works with subteams..')
            return

        if self.is_registered_company(team_name) and not force:
            return await self.reply(event, f'{team_name} has already been registed')

        response = await self.register_company(team_name, force=force)

        for msg in response.get('messages', []):
            await self.reply(event, msg)

    async def do_kill(self, event: pykeybasebot.kbevent.KbEvent):
        # check_existent_employee(self, event)
        # self.reply(event, f"Good bye cruel world")

        for msgs in self.messages.values():
            for value in msgs:
                message = getattr(value, 'message', None)
                message = getattr(value, 'msg', None) if not message else message
                msg_id = getattr(message, 'id', None)
                try:
                    await self.delete(message.channel, msg_id)
                except BaseException as excp:
                    val = value.to_dict()
                    val['_message'] = val.pop('msg', {})
                    self.log.exception(excp, extra=val)

        pid = getpid()
        process = psutil.Process(pid)
        process.terminate()

        for _ in range(2):
            if process.is_running():
                sleep(1)
            else:
                break

        if process.is_running():
            process.kill()

    def add_default_commands(self):
        self.add_command(self.is_team_invite, self.leave_team)
        self.add_command(self.is_a_hi, self.do_welcome)
        self.add_command(self.is_a_leave, self.leave_team)
        self.add_command(self.is_a_brb, self.do_break)
        self.add_command(self.is_a_oops, self.do_fix)
        self.add_command(self.is_a_bye, self.do_bye)
        self.add_command(self.is_a_back, self.do_back)
        self.add_command(self.is_a_exit, self.do_kill)
        self.add_command(self.is_a_register, self.do_register)


pingbot = CustomBot(
    botname=os.environ.get('BOTNAME'),
    paperkey=os.environ.get('PAPERKEY'),
    # channels=[],
)


pingbot.add_default_commands()
pingbot.run()
