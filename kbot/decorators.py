from functools import partial, update_wrapper

from pykeybasebot.types import chat1


class IsSystemMsg:
    def __init__(self, func):
        update_wrapper(self, func)
        self.func = func

    def __get__(self, obj, objtype):
        """Support instance methods."""
        return partial(self.__call__, obj)

    def __call__(self, obj, event, *args, **kwargs):
        if event.msg.content.type_name != chat1.MessageTypeStrings.SYSTEM.value:
            return False
        return self.func(obj, event, *args, **kwargs)


class IsChatMsg:
    def __init__(self, func):
        update_wrapper(self, func)
        self.func = func

    def __get__(self, obj, objtype):
        """Support instance methods."""
        return partial(self.__call__, obj)

    def __call__(self, obj, event, *args, **kwargs):
        if event.msg.content.type_name != chat1.MessageTypeStrings.TEXT.value:
            return False
        return self.func(obj, event, *args, **kwargs)
