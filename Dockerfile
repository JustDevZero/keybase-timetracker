FROM python-keybase

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y wget curl virtualenv \
    # && apt-get install -y wget curl python3.8-dev virtualenv \
    && chown -R keybase:keybase /home/keybase \
    && update-alternatives --install /usr/bin/python python $(which python3.9) 1 \
    && rm -rf /var/lib/apt/lists/*

USER keybase

ENV VIRTUAL_ENV=/home/keybase/virtualenv \
    PATH=/home/keybase/virtualenv/bin:$PATH \
    KEYBASE_SERVICE=1

RUN virtualenv --python=python3.9 /home/keybase/virtualenv \
    && pip install ipdb GitPython psutil setproctitle \
    && pip install git+https://github.com/JustDevZero/pykeybasebot \
    && pip install --upgrade dataclasses-json \
    && echo 'export PATH=/home/keybase/virtualenv/bin:$PATH' >> ~/.bashrc \
    && echo 'export VIRTUAL_ENV=/home/keybase/virtualenv' >> ~/.bashrc

WORKDIR /home/keybase/project
ENTRYPOINT [ "/home/keybase/project/entrypoint.sh" ]
# CMD ["/home/keybase/virtualenv/bin/python", "/home/keybase/project/kbot/bot.py"]
